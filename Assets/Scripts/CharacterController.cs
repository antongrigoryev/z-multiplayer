﻿using UnityEngine;

public class CharacterController : MonoBehaviour 
{
    [SerializeField]
    private float moveSpeed = 5;

    public int Score { get; private set; }

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update () 
    {
        if (Input.GetMouseButton(0))
        {
            animator.SetBool("move", true);

            var mousePos = Input.mousePosition;
            var objectPos = Camera.main.WorldToScreenPoint(transform.position);
            Vector2 targetDir = (mousePos - objectPos).normalized;
            float angle = -Mathf.Atan2(targetDir.x, targetDir.y) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            Vector3 shift = targetDir * moveSpeed * Time.deltaTime;
            transform.position += shift;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            animator.SetBool("move", false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("fruit"))
        {
            Score++;
        }
    }
}
