﻿using System;
using Mp.Extensions;
using UnityEngine;

namespace Mp
{
    public sealed class Fruit : MonoBehaviour
    {
        public event Action<Fruit> Collected;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<CharacterController>() != null)
            {
                Collected.SafeInvoke(this);
                Destroy(gameObject);
            }
        }
    }
}

