﻿using UnityEngine;

namespace Mp
{
    public class FruitFabric : MonoBehaviour
    {
        [SerializeField]
        private Fruit fruitPrefab;

        [SerializeField]
        private int maxCount = 10;

        [SerializeField]
        private float fieldSize;

        private int currentCount = 0;

        // Update is called once per frame
        private void Update()
        {
            while (currentCount < maxCount)
            {
                InstantiateFruit();
                currentCount++;
            }
        }

        private void OnFruitCollected(Fruit fruit)
        {
            fruit.Collected -= OnFruitCollected;
            currentCount--;
        }

        private void InstantiateFruit()
        {
            var x = Random.Range(-fieldSize / 2, fieldSize / 2);
            var y = Random.Range(-fieldSize / 2, fieldSize / 2);
            var pos = new Vector2(x, y);
            var fruit = Instantiate(fruitPrefab);
            fruit.transform.position = pos;
            fruit.Collected += OnFruitCollected;
        }
    }
}