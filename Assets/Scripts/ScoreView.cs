﻿using UnityEngine;
using UnityEngine.UI;

namespace Mp.Ui
{
    public sealed class ScoreView : MonoBehaviour
    {
        [SerializeField]
        private CharacterController character;

        private Text text;

        private void Awake()
        {
            text = GetComponent<Text>();
        }

        private void Update()
        {
            text.text = character.Score.ToString();
        }
    }
}

