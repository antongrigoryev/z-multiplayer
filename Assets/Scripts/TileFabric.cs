﻿using UnityEngine;

namespace Mp.Map
{
    public sealed class TileFabric : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer tile;

        [SerializeField]
        private int width;

        [SerializeField]
        private int height;

        private void Awake()
        {
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    var position = new Vector2(i - width / 2, j - height / 2);
                    var sprite = Instantiate(tile);
                    sprite.transform.parent = transform;
                    sprite.transform.localPosition = position;
                }
            }
        }
    }
}

